package cosmeticosyanina;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CosmeticosYanina {

    public static void main(String[] args) {
        System.out.println("Hola");

        String consultaSql = "SELECT * FROM `productos`";
        try {
            Connection miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);
            ResultSet miresultado = miPreparativo.executeQuery();
            while (miresultado.next()) {
                System.out.println(miresultado.getString("nombre"));
            }

        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (SQLException ex) {
            System.out.println(ex);
        }

        //DB misProductos = new DB();
        //System.out.println(misProductos.productos());
        System.out.println("Chau");
    }

}
